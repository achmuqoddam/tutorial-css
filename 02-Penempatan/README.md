## Menempatkan CSS

- Bagaimana cara menenmpatkan CSS didalam dokumen HTML

## Ada 3 cara menempatkan CSS pada HTML

- Embed
- Inline
- External

## Embed

- Salah satu cara menempatkan CSS langsung di dalam dokumen HTML di bagian dalam tag head

## Inline

- Sama halnya dengan cara di Embed tapi perbedaannya yaitu tag HTML yang ingin memakai CSS menggunakan atribut style pada HTML, bukan di bagian dalam tag head

## External

- Membuat file CSS terpisah dan di hubungkan ke dokumen HTML
