## CSS (Cascading Style Sheet)

## Definisi:

- Aturan yang digunakan untuk menampilkan elemen / tag HTML
- Mekanisme sederhana yang mengatur gaya / style pada halaman web (www.w3.org/style/CSS)

## Fungsi:

- Dibuat terpisah dengan HTML

## Tujuan:

- Untuk memisahkan konten dan style
- 1 CSS dapat digunakan untuk banyak halaman HTML (Teknik Cascading)
- 1 Halaman HTML dapat terlihat berbeda jika menggunakan CSS yang berbeda pula (Teknik Cascading)
