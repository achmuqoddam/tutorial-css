## Anatomy / Struktur dari CSS

- selector { property: value; }

## Selector

- Digunakan untuk memilih dan memanipulasi elemen spesifik pada HTML
- Elemen HTML dipilih berdasarkan tag, id, class, bahkan pola / pattern
- Semakin kompleks struktur HTML, selector juga bisa semakin kompleks / spesifik

## Property

- Jenis-jenis property banyak, tergantung penyesuaian tujuan dan fungsi nya

## Value

- value adalah isi dari property
